# Free Software Supporter
Issue 123, July 2018

Welcome to the Free Software Supporter, the Free Software Foundation's
(FSF) monthly news digest and action update -- being read by you and
190,022 other activists. That's 1,880 more than last month!

### Show your support for free software: become a member or donate today!

*From June 15*

Your activism drives the free software movement. Together, we have
been proactively building a future where computer users are in
control, while also reacting to immediate threats to our digital
freedoms. Our associate membership program provides crucial, ongoing
support that ensures the FSF's financial stability, making our work
possible. Will you take the next step and join us as an associate
member or make a one-time donation today?

 * <https://www.fsf.org/blogs/community/get-the-latest-in-libre-from-the-fsf-bulletin>

## TABLE OF CONTENTS

* Take action on the International Day Against DRM this September 18th 
* The FSF tech team needs a Fall 2018 intern! Apply by July 22nd, 2018 
* How to defend your encrypted emails against prying eyes 
* European Union Public License v. 1.2 added to license list 
* "CLASSICS Act" continues through US Congress, decreasing your access to music
* Free software is at risk in the EU -- take action now 
* Copyright Directive: Implementation of generalized and automated upload filtering on the Internet; decisive vote in July
* Action plan against the first obligatory EU internet filter
* These MEPs voted to restrict the Internet in Europe today -- but we’re not giving up
* Opening the doors of the Software Heritage archive
* A case for the total abolition of software patents
* Amazon needs to stop providing facial recognition tech for the government
* The path to victory on net neutrality in the House of Representatives and how you can help
* About BLAG's removal from our list of endorsed distributions 
* Xapian joins Conservancy as a member project
* Conservancy welcomes Racket as its newest member project
* Introducing Sonali, Outreachy summer intern with Free Software Foundation 
* Introducing David Hedlund, intern with the FSF tech team 
* Join the FSF and friends in updating the Free Software Directory
* LibrePlanet featured resource: End Software Patents
* GNU Spotlight with Mike Gerwitz: 18 new GNU releases!
* GNU Toolchain update: Support GNU Toolchain
* Richard Stallman's speaking schedule and other FSF events
* Thank GNUs!
* GNU copyright contributions
* Take action with the FSF!

View this issue online here: <https://www.fsf.org/free-software-supporter/2018/july>

Encourage your friends to subscribe and help us build an audience by
adding our subscriber widget to your Web site.

  * Subscribe: <https://www.fsf.org/free-software-supporter>
  * Widget: <https://www.fsf.org/associate/widget>

Miss an issue? You can catch up on back issues at
<https://www.fsf.org/free-software-supporter>.

###

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui: <https://www.fsf.org/free-software-supporter/2018/julio>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici: <https://www.fsf.org/free-software-supporter/2018/juillet>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em Português. Para ver a
versão em Português, clique aqui: <https://www.fsf.org/free-software-supporter/2018/julho>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em Português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

#

### Take action on the International Day Against DRM this September 18th 

Join the Defective by Design (DBD) crew and celebrate International
Day Against DRM (IDAD) this September 18th! DBD raises awareness about
devices and media encumbered by Digital Restrictions Management (DRM),
showing what they really are: Defective by Design. DRM is the practice
of placing technological restrictions on digital media, and we're
working together to eliminate it as a threat to freedom for computer
users, as well as a threat to privacy for readers, viewers, and those
making art, media, and more.

We're looking for vendors of DRM-free media, organizations that
support the building of a DRM-free world, and those who believe in the
mission of DBD to participate by offering sales, writing blog posts,
organizing events, and sharing with your members about IDAD. Please
contact us at <info@defectivebydesign.org> for more information.

 * <https://www.defectivebydesign.org/dayagainstdrm>

### The FSF tech team needs a Fall 2018 intern! Apply by July 22nd, 2018 

*From June 13*

 This is an educational opportunity to work with the organization that
 sponsors the GNU Project, publishes the GNU General Public License
 (GPL), and fights for software freedom. As a fall intern, you will
 work closely with the FSF tech team in your area of interest, such as
 network administration, GNU Project support, or Web development.

 * <https://www.fsf.org/blogs/sysadmin/the-fsf-tech-team-needs-a-fall-2018-intern-apply-by-july-22nd-2018>

### How to defend your encrypted emails against prying eyes 

*From June 7*

In May, a draft technical paper published at efail.de recommended that
people stop using GNU Privacy Guard (GPG) plugins to encrypt their
email. At the same time, the Electronic Frontier Foundation
raised the alarm about seemingly new vulnerabilities in GPG, echoing
the paper's cautionary recommendations. Much of this information isn't
new. The issue isn't a flaw in GPG, and there is no need to panic or
discontinue using GPG, including for signing emails or for encrypting
and decrypting files outside of your email client. Here are the facts.

 * <https://www.fsf.org/blogs/community/how-to-defend-your-encrypted-emails-against-prying-eyes>

### European Union Public License v. 1.2 added to license list 

*From June 26*

We recently added the EUPL-1.2 to our list of Various Licenses and
Comments About Them. This list helps users to understand whether a
particular license is a free software license, and whether it is
compatible with the GNU GPL. Like the
previous version of the EUPL (EUPL-1.1), the EUPL-1.2 is included in
the section for free licenses that are GNU GPL-incompatible, but with
an important caveat. While the EUPL-1.2's copyleft by itself is
incompatible with the GNU GPL, the license provides a few mechanisms
for re-licensing which enable combination with GNU GPL-licensed works.

 * <https://www.fsf.org/blogs/licensing/european-union-public-license-v-1-2-added-to-license-list>

### "CLASSICS Act" continues through US Congress, decreasing your access to music

*From June 26*

The 1990s have come back to haunt us as the US Congress attempts to
extend the scope of copyright law again, just two decades after the
last extension (which added twenty years to the terms of existing
copyrights at the time), and this time they set their sights on the
Internet. We urged our supporters to call their Senators to shut down
this attempt to narrow your access to music online, but unfortunately,
the bill was approved by the Senate Judiciary Committee, taking the fight to the Senate floor.

 * <https://www.fsf.org/blogs/community/take-action-tell-us-congress-not-to-pass-the-classics-act>

### Free software is at risk in the EU -- we asked you to take action

*From June 18*

Free software is under attack within the European Union (EU). The
proposed European Copyright Directive poses a great risk for free
software and its development. We urged our European supporters to
contact their members of European Parliament to encourage them to vote
against this incredibly destructive measure.

 * <https://www.fsf.org/blogs/community/free-software-is-at-risk-in-the-eu-take-action-now>

### Copyright Directive: Implementation of generalized and automated upload filtering on the Internet; decisive vote in July

*From June 20 by April*

On June 20th, the Legal Affairs (JURI) Committee adopted the draft
Copyright Directive, including its Article 13 that imposes the
implementation of generalized and automated upload filters by hosting
platforms. While free software development platforms are exempted from
this filtering obligation, the very idea of it is disastrous. April
calls on Members of the European Parliament (MEPs) to reject this
draft directive in its entirety during the July plenary.

  * <https://www.april.org/en/copyright-directive-implementation-generalized-and-automated-upload-filtering-internet-decisive-vote>

### Action plan against the first obligatory EU internet filter

*From June 28 by Andreea Belu*

In the next months, our main focus will be on ensuring that the
position of the whole EU Parliament is against censorship
machines. For this to happen, we will address all 750 MEPs around key
moments. Below you will find an easy, yet detailed explanation of the
opportunities we will have to challenge the censorship machine as the
law makes its way through the EU decision-making process.

  * <https://edri.org/strategy-against-the-first-obligatory-eu-internet-filter/>

### These MEPs voted to restrict the Internet in Europe today -- but we’re not giving up

*From June 20 by Julia Reda*

MEPs on the Legal Affairs Committee of the European Parliament were
asked to decide: should your freedom to participate on the Web be
restricted to serve corporate interests -- or should alternative
measures be adopted that safeguard fundamental rights? Despite a
massive outpouring of protest from voters during these last few days,
the majority voted for both the link tax and upload filters. The next
vote on this measure will happen on July 4, and free software
supporters and other crusaders for Internet freedom are still
determined to fight back.

 * <https://juliareda.eu/2018/06/not-giving-up>

### Opening the doors of the Software Heritage archive

*From June 7 by Software Heritage*

The mission of Software Heritage is to collect, preserve, and make
readily available the source code of all software ever written,
building an essential infrastructure at the service of cultural
heritage, science, industry, and society as a whole. On June 7, the
Software Heritage team unveiled a major milestone in their roadmap:
the grand opening of the doors of [their
archive](https://www.softwareheritage.org/archive/) to the public,
allowing you to explore the largest collection of software source code
in the world!

 * <https://www.softwareheritage.org/2018/06/07/opening-the-door/> 

### A case for the total abolition of software patents

*From June 6 by Gervase Markham*

Software patents should be abolished first and foremost because they
are an infringement on individual freedoms like free speech. But they
also don't even come close to achieving their stated purpose of
encouraging advancements in software development. This paper makes a
strong step-by-step argument showing that they don't.

 * <https://gerv.net/writings/patents/>

### Amazon needs to stop providing facial recognition tech for the government

*From June 21 by Evan Selinger*

Imagine a technology that is potently, uniquely dangerous  --
something so inherently toxic that it deserves to be completely
rejected, banned, and stigmatized. Something so pernicious that
regulation cannot adequately protect citizens from its effects. That
technology is already here. It is facial recognition technology, and
its dangers are so great that it must be rejected entirely.

  * <https://medium.com/s/story/amazon-needs-to-stop-providing-facial-recognition-tech-for-the-government-795741a016a6>

### The path to victory on net neutrality in the House of Representatives and how you can help

*From May 18 by Ernesto Falcon*

The United States Senate has voted to overturn the FCC and restore net
neutrality protections, the fate of that measure currently rests in
the House of Representatives. While many will think that the uphill
battle there makes it a lost cause, that is simply not true. Together,
we have the power to win in the House of Representatives. (This article is a little older, but it has great information about how and why to contact your representatives to contribute to this fight.)

  * <https://www.eff.org/deeplinks/2018/05/path-victory-net-neutrality-house-representatives-and-how-you-can-help>

### About BLAG's removal from our list of endorsed distributions 

*From June 22*

The maintainers of BLAG requested that they be removed from our list
of free distros, as the distribution is no longer maintained. While it
is always sad to see a free distribution close up shop, we can all
still be thankful for the maintainers' work over the years, and that
there are still many free distributions available. Users of BLAG
should consider switching to another distro on our list to ensure that
the security of their system is up to date.

  * <https://www.fsf.org/blogs/licensing/about-blags-removal-from-our-list-of-endorsed-distributions>

### Xapian joins Conservancy as a member project

*From June 19 by Software Freedom Conservancy*

Software Freedom Conservancy proudly welcomes Xapian as Conservancy's
newest member project. Xapian is a probabilistic information retrieval
library that allows developers to add advanced indexing and search
facilities to their own applications.

  * <https://sfconservancy.org/news/2018/jun/19/xapianjoins/>

### Conservancy welcomes Racket as its newest member project

*From June 12 by Software Freedom Conservancy*

Software Freedom Conservancy and the Racket community are pleased to
announce that Racket is Conservancy's newest member project. Racket is
a general-purpose programming language as well as the world’s first
ecosystem for developing and deploying new languages. Racket comes
with special support for novices and for on-boarding
beginners. Several popular online learning platforms include Racket
courses. The Realm of Racket is also a great place for programmers who
want to become familiar with the basics of the language.

  * <https://sfconservancy.org/news/2018/jun/12/racketjoins/>

### Introducing Sonali, Outreachy summer intern with Free Software Foundation 

*From June 13 by Sonali Singhal*

I am a first year college student. This is my first experience with a
free software organization. I am glad that I got introduced to the FSF
so early in my career. I have worked on the Free Software Directory
for about 2 months now, and have made 11 contributions so far and over
700 major and minor edits.

 * <https://www.fsf.org/blogs/sysadmin/introducing-sonali-outreachy-summer-intern-with-free-software-foundation>

### Introducing David Hedlund, intern with the FSF tech team 

*From June 13 by David Hedlund*

I joined the FSF as an intern starting May 14, 2018, and will work
through August 14, 2018 to fix bugs and work on a data import program
that will import data from addons.mozilla.org and Debian main. I will
be blogging about every two weeks, and I will go into more detail
about my work in those posts.

 * <https://www.fsf.org/blogs/sysadmin/introducing-david-hedlund-intern-with-the-fsf-tech-team>

### Join the FSF and friends in updating the Free Software Directory

Tens of thousands of people visit directory.fsf.org each month to
discover free software. Each entry in the Directory contains a wealth
of useful information, from basic category and descriptions to version
control, IRC channels, documentation, and licensing. The Free Software
Directory has been a great resource to software users over the past
decade, but it needs your help staying up-to-date with new and
exciting free software projects.

To help, join our weekly IRC meetings on Fridays. Meetings take place
in the #fsf channel on irc.freenode.org, and usually include a handful
of regulars as well as newcomers. Freenode is accessible from any IRC
client -- Everyone's welcome!

The next meeting is Friday, July 6, from 12pm to 3pm EDT (16:00 to
19:00 UTC). Details here:

  * <https://directory.fsf.org/wiki/Free_Software_Directory:Meetings>

### LibrePlanet featured resource: End Software Patents

Every month on LibrePlanet, we highlight one resource that is
interesting and useful -- often one that could use your help.

For this month, we are highlighting End Software Patents, which
provides information about the campaign to abolish software patents,
globally. You are invited to adopt, spread and improve this important
resource.

  * <https://libreplanet.org/wiki/Group:End_Software_Patents>

Do you have a suggestion for next month's featured resource? Let us
know at <campaigns@fsf.org>.

### GNU Spotlight with Mike Gerwitz: 18 new GNU releases!

* [bison-3.0.5](https://www.gnu.org/software/bison/)
* [dfarc-3.14](https://www.gnu.org/software/freedink/)
* [dr-geo-18.06](https://www.gnu.org/software/dr-geo/)
* [emacs-26.1](https://www.gnu.org/software/emacs/)
* [foliot-0.9.8](https://www.gnu.org/software/foliot/)
* [freedink-3.14](https://www.gnu.org/software/freedink/)
* [gdbm-1.15](https://www.gnu.org/software/gdbm/)
* [gnuastro-0.6](https://www.gnu.org/software/gnuastro/)
* [gnuhealth-3.2.10](http://health.gnu.org/)
* [gnupg-2.2.8](https://www.gnu.org/software/gnupg/)
* [gnu-pw-mgr-2.3.1](https://www.gnu.org/software/gnu-pw-mgr/)
* [gsl-2.5](https://www.gnu.org/software/gsl/)
* [libgcrypt-1.8.3](https://www.gnu.org/software/libgcrypt/)
* [linux-libre-4.17.2-gnu](https://www.gnu.org/software/linux-libre/)
* [nano-2.9.8](https://www.gnu.org/software/nano/)
* [parallel-20180622](https://www.gnu.org/software/parallel/)
* [unifont-11.0.01](https://www.gnu.org/software/unifont/)
* [units-2.17](https://www.gnu.org/software/units/)

For announcements of most new GNU releases, subscribe to the info-gnu
mailing list: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

To download: nearly all GNU software is available from
<https://ftp.gnu.org/gnu/>, or preferably one of its mirrors from
<https://www.gnu.org/prep/ftp.html>. You can use the URL
<https://ftpmirror.gnu.org/> to be automatically redirected to a
(hopefully) nearby and up-to-date mirror.

A number of GNU packages, as well as the GNU operating system as a
whole, are looking for maintainers and other assistance: please see
<https://www.gnu.org/server/takeaction.html#unmaint> if you'd like to
help. The general page on how to help GNU is at
<https://www.gnu.org/help/help.html>.

If you have a working or partly working program that you'd like
to offer to the GNU project as a GNU package, see
<https://www.gnu.org/help/evaluation.html>.

As always, please feel free to write to us at <maintainers@gnu.org>
with any GNUish questions or suggestions for future installments.

### GNU Toolchain update: Support GNU Toolchain

Donate to support the GNU Toolchain, a collection of foundational
freely licensed software development tools including the [GNU C
Compiler collection (GCC)](https://gcc.gnu.org/), the [GNU C Library
(glibc)](https://www.gnu.org/software/libc/libc.html), and the [GNU
Debugger (GDB)](https://sourceware.org/gdb/).

* <https://my.fsf.org/civicrm/contribute/transact?reset=1&id=57>

### Richard Stallman's speaking schedule

For event details, as well as to sign-up to be notified for future
events in your area, please visit <https://www.fsf.org/events>.

So far, Richard Stallman has the following events this month:

  * July 22, 2018, New York, NY, ["We must legislate to block collection of personal data"](https://www.fsf.org/events/rms-20180722-newyork-hope)

### Other FSF and free software events

  * July 20-22, 2018, New York, NY, [Karen M. Sandler and Molly de Blanc, "Introduction to User Freedom"](https://www.fsf.org/events/karen-sandler-molly-deblanc-20180720-newyork-hope)
  * November 23-25, 2018, Las Palmas de Gran Canaria, Spain, [GNU Health Con 2018](https://www.fsf.org/events/event-20181123-laspalmas-gnuhealthcon)

### Thank GNUs!

We appreciate everyone who donates to the Free Software Foundation,
and we'd like to give special recognition to the folks who have
donated $500 or more in the last month.

  * <https://www.gnu.org/thankgnus/2018supporters.html>

This month, a big Thank GNU to:

* Christian Sperr 
* Elliot Rosenberger
* Minoru Sekine
* Mirko Lüdde
* Ravi Swamy
* René Genz
* Shawn C [ a.k.a "citypw"]
* Thomas Weeks III

You can add your name to this list by donating at
<https://donate.fsf.org/>.

### GNU copyright contributions

Assigning your copyright to the Free Software Foundation helps us
defend the GPL and keep software free. The following individuals have
assigned their copyright to the FSF in the past month:

* Amin Bandali (GNU) (Emacs)
* Lucas Werkmeister (GNU) (Emacs) 

Want to see your name on this list? Contribute to GNU and assign your
copyright to the FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Take action with the FSF!

Contributions from thousands of individual members enable the FSF's
work. You can contribute by joining at <https://my.fsf.org/join>. If
you're already a member, you can help refer new members (and earn some
rewards) by adding a line with your member number to your email
signature like:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

The FSF is always looking for volunteers
(<https://www.fsf.org/volunteer>). From rabble-rousing to hacking,
from issue coordination to envelope stuffing -- there's something
here for everybody to do. Also, head over to our campaigns section
(<https://www.fsf.org/campaigns>) and take action on software
patents, DRM, free software
adoption, OpenDocument, Recording Industry Association of America
(RIAA), and more.


###

Copyright © 2018 Free Software Foundation, Inc.

This work is licensed under the Creative Commons Attribution 4.0
Unported License. To view a copy of this license, visit
<https://creativecommons.org/licenses/by/4.0/>.

