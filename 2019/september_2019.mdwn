# Free Software Supporter
Issue 137, September 2019

*Please add <info@fsf.org> to your address book to make sure you receive future emails from us.*

Welcome to the *Free Software Supporter*, the Free Software
Foundation's (FSF) monthly news digest and action update -- being read
by you and 211,363 other activists. That's 1,098 more than last month!

### Join us for the International Day Against Digital Restrictions Management (DRM) on October 12

*From September 4*

[Defective by Design](https://www.defectivebydesign.org/) is calling
on you to stand up against digital restrictions management and join us
in celebration of the International Day Against DRM (IDAD) on October
12th, 2019. Keep an eye on [defectivebydesign.org](https://www.defectivebydesign.org/)
for further announcements.

## TABLE OF CONTENTS

* Alexandre Oliva joins Free Software Foundation board of directors 
* Early registration open for FSF's licensing seminar on October 16 in Raleigh, NC 
* RMS travels in May 2019: Photos from Aalborg and Copenhagen 
* US National Security Agency (NSA) whistleblower Edward Snowden wants to help combat surveillance
* Your car talks to the manufacturer. Advocates want it to talk to you, too.
* Poland brings the fight against upload filters to the European Court of Justice
* Things you didn't know about GNU Readline
* HOPE needs your help
* Amazon really doesn't give a (bleep), huh?
* Everything you need to know about Ring, Amazon's surveillance camera company
* Crowdfunding campaign for "Hacking for the Commons" or "La bataille du Libre"
* ActivityPub Conference is being held this September in Prague, Czechia
* GNOME West Coast Hackfest 2019 summary
* So-called "smart" ovens have been turning on overnight and preheating to 400 degrees
* GCC 9.2 released
* FSF hosting a mirror of ProteanOS
* GNU Emacs text editor version 26.3 now available
* August GNU Emacs news 
* Join the FSF and friends in updating the Free Software Directory
* LibrePlanet featured resource: Group:IceCat/Links to distributions
* GNU Spotlight with Mike Gerwitz: 15 new GNU releases!
* FSF events and other free software events
* Thank GNUs!
* GNU copyright contributions
* Translations of the *Free Software Supporter*
* Take action with the FSF!

View this issue online here:
<https://www.fsf.org/free-software-supporter/2019/september>

Encourage your friends to subscribe and help us build an audience by
adding our subscriber widget to your Web site.

  * Subscribe: <https://www.fsf.org/free-software-supporter/subscribe>
  * Widget: <https://www.fsf.org/associate/widget>

Miss an issue? You can catch up on back issues at
<https://www.fsf.org/free-software-supporter>.

Want to read this newsletter translated into another language? Scroll
to the end to read the *Supporter* in French, Spanish, or Portuguese.

***

### Alexandre Oliva joins Free Software Foundation board of directors 

*From August 28*

A longtime free software activist and founder of [FSF Latin
America](https://www.fsfla.org/ikiwiki/), Oliva brings decades of
experience in the free software movement to the FSF board. In the
community, he is held in especially high regard for being the chief
developer of the [GNU Linux-libre
project](https://www.fsfla.org/ikiwiki/selibre/linux-libre/), a
version of the kernel Linux that removes all nonfree bits from the
kernel's source code, enabling users around the world to run fully
free versions of the GNU/Linux operating system, and is a program of
vital importance in the cause for software freedom. For his deep
commitment and tireless work in free software, Oliva was the recipient
of the [2016 Advancement of Free Software
award](https://www.fsf.org/news/securedrop-and-alexandre-oliva-are-2016-free-software-awards-winners)
given annually by the FSF.

 * <https://www.fsf.org/news/alexandre-oliva-joins-free-software-foundation-board-of-directors>

### Early registration open for FSF's licensing seminar on October 16 in Raleigh, NC 

*From August 29*

The Free Software Foundation's (FSF) Licensing and Compliance Lab is
proud to announce that [registration is now
open](https://www.fsf.org/events/cle-seminar-20191016-raleigh) for the
continuing legal education (CLE) seminar on GPL Enforcement and Legal
Ethics, which will be held on Wednesday, October 16, 2019, at the
Raleigh Convention Center in North Carolina.

 * <https://www.fsf.org/news/early-registration-open-for-fsfs-licensing-seminar-on-oct-16-in-raleigh-nc>

### RMS travels in May 2019: Photos from Aalborg and Copenhagen 

*From July 31*

Free Software Foundation president Richard Stallman (RMS) was in
Denmark in May 2019. After a visit to the beach in nearby Slettestrand
the day before, RMS went to Aalborg, where he delivered his speech
“Free software and your freedom” at Aalborg University (AAU), on May
6.

 * <https://www.fsf.org/blogs/rms/photo-blog-2019-may-aalborg-copenhagen>

### US National Security Agency (NSA) whistleblower Edward Snowden wants to help combat surveillance

*From August 3 by Aaron Kesel*

Whistleblower Edward Snowden is back in the news, announcing plans for
a memoir as well as his opposition to corporate surveillance by
proprietary software and Service as a Software Substitute companies like
Instagram, Facebook, and YouTube. This is a good time to revisit his
LibrePlanet 2016 keynote speech at
<https://media.libreplanet.org/u/libreplanet/m/libreplanet-2016-the-last-lighthouse-3d51/>.

 * <https://www.activistpost.com/2019/08/nsa-whistleblower-edward-snowden-says-facebook-instagram-spying-on-customers-wants-to-help-combat-surveillance.html>

### Your car talks to the manufacturer. Advocates want it to talk to you, too.

*From August 6 by Adrian Ma*

If your car is less than a decade old, chances are it’s equipped with
“telematics”-- basically, computers that let the manufacturer
wirelessly track a car’s performance and send notifications when
something needs fixing. In some cases, these notifications may even
recommend a nearby dealership to make those repairs. The Massachusetts
Right to Repair Coalition is proposing that automakers be required to
give car owners access to their real-time car data, which is a great
idea -- except that their proposal is to do this through a nonfree
mobile app. Yuck!

This is part of the oppressive practice of the Internet of Stings (our
improved name for the "Internet of Things"): making you communicate
with the products you own via the manufacturer's server. The right
thing to require is that the car offer a data-only interface, like a
slot to put a memory chip into and the car will write the data on
it. Then you could show that data to an independent repair shop when
you want to, even if you disconnected the cellular data antenna and
the GPS antenna just after you bought the car. We applaud any effort
toward user freedom, but let's make sure it's done the right way!

 * <https://www.wbur.org/bostonomix/2019/08/06/right-to-repair-ballot-measure>

### Poland brings the fight against upload filters to the European Court of Justice

*From August 26 by Étienne Gonnu*

Poland is challenging in the European Court of Justice (EUCJ) the
legality of Article 17 of the Copyright Directive, which was voted in
March 2019 by the European Parliament. More specifically, the member
state is arguing that the provisions imposing upload filters are
contrary to Article 11 of the Charter of Fundamental Rights of the
European Union. In other words, the provisions disproportionately
infringe on freedom of speech, a stance that April has regularly
defended, along with many other organizations.

 * <https://www.april.org/en/poland-brings-fight-against-upload-filters-european-court-justice>

### Things you didn't know about GNU Readline

*From August 22 by Chet Ramey*

GNU Readline is an unassuming little software library that I relied on
for years without realizing that it was there. Tens of thousands of
people probably use it every day without thinking about it. If you use
the Bash shell, every time you auto-complete a filename, or move the
cursor around within a single line of input text, or search through
the history of your previous commands, you are using GNU Readline.

 * <https://twobithistory.org/2019/08/22/readline.html>

### HOPE needs your help

*From August 1*

The HOPE conference -- Hackers on Planet Earth -- needs your help!
Hotel costs are making it prohibitive to host the conference, and they
need your suggestions to make sure this event can keep
happening. Email <hope@hope.net> with your suggestions.

 * <https://hope.net/>

### Amazon really doesn't give a (bleep), huh?

*From August 2 by Adam Clark Estes*

Yes, actual people are listening to what your voice assistant records,
including your most private moments, and the extent to which Amazon,
Apple, and Google will let you opt out varies. Microsoft contractors
are also [listening to your Skype
calls](https://www.vice.com/en_us/article/xweqbq/microsoft-contractors-listen-to-skype-calls). And
of course none of this is remotely transparent -- because you're not
allowed to examine how the proprietary software that controls how these
devices work.

 * <https://gizmodo.com/amazon-really-doesn-t-give-a-shit-huh-1836912275>

### Everything you need to know about Ring, Amazon's surveillance camera company

*From August 8 by Caroline Haskins*

How does Ring partner with police? Are there privacy concerns with
Ring doorbells? What is the Neighbors app? What functions are hiding
in the Ring's proprietary software that you can't examine or control?
Learn what *Motherboard* discovered when they perused 1,800 pages of
public records.

 * <https://www.vice.com/en_us/article/qvg48d/everything-you-need-to-know-about-ring-amazons-surveillance-camera-company?xyz>

### Crowdfunding campaign for "Hacking for the Commons" or "La bataille du Libre"

*From August 14*

At the heart of technology, two rationales are now clashing: since the
80s, the emancipatory principles of free software movements and free
culture movements began to attack the exclusive and “private” ones
defending intellectual property rights. Free software, free seeds,
free medicines, free knowledge: focusing on freedom, cooperation and
sharing, these movements aim to restore the user’s autonomy and power,
leading to a world free of patents, for the benefit of the common
good.

The documentary "Hacking for the Commons" or "La bataille du Libre"
addresses all of these issues, but they need your help: to find out
how to contribute to their crowdfunding campaign without using
proprietary JavaScript, email <contact@labatailledulibre.org>!

 * <https://www.labatailledulibre.org/en/the-movie/>

### ActivityPub Conference is being held this September in Prague, Czechia

*From July 22 by Christopher Lemmer Webber*

ActivityPub Conference provides you with an opportunity to discuss the
present and future of ActivityPub, the world's leading federated
social Web standard. This two-day event will include presentations of
prepared talks on Saturday, followed by a loosely structured
"unconference" on Sunday.

We're told that all tickets have been booked, but if you're interested
in learning where ActivityPub is headed, keep an eye on their Web
site: sessions will be recorded, and may also be streamed live.

 * <https://dustycloud.org/blog/activitypub-conf-2019/>

### GNOME West Coast Hackfest 2019 summary

*From August 14 by GNOME Foundation*

The West Coast 2019 Hackfest, which took place in Portland, Oregon, US
between July 18-21, gathered members from several teams including
coders, members of the documentation team, and the engagement
team. The 2019 West Coast Hackfest was generally agreed as an
successful event by the participants. Links to blogs with more details
about the work done can be found on the [West Coast Hackfest wiki
page](https://wiki.gnome.org/Hackfests/WestCoastHackfest). The GNOME
Foundation wants to thank all the individuals who made the event such
a success!

 * <https://www.gnome.org/news/2019/08/west-coast-hackfest-2019-summary/>

### So-called "smart" ovens have been turning on overnight and preheating to 400 degrees

*From August 14 by Ashley Carman*

At least three "smart" June Ovens have turned on in the middle of the
night and heated up to 400 degrees Fahrenheit or higher. Given that 33
percent of home fires are caused by what the New York City Fire
Department calls "unattended cooking," this is no small danger!

Of course, practical snafus are only one reason why we oppose
proprietary software in "the Internet of Things." The fact that owners
aren't permitted to tinker with these "smart" ovens and fix the
problem on their own isn't just an annoyance, or even a danger to
their homes and lives: it's a violation of their rights.

 * <https://www.theverge.com/2019/8/14/20802774/june-smart-oven-remote-preheat-update-user-error>

### GCC 9.2 released

*From August 12 by GCC*

The GNU Project and the GCC developers are pleased to announce the
release of GCC 9.2. This release is a bug-fix release, containing
fixes for regressions in GCC 9.1 relative to previous releases of GCC.

 * <https://gcc.gnu.org/gcc-9/>

### FSF hosting a mirror of ProteanOS

*From August 7*

We are now hosting a mirror of ProteanOS at
<https://mirror.fsf.org/proteanos>. ProteanOS is one of the free
GNU/Linux distributions listed
[here](https://www.gnu.org/distros/free-distros.html).

 * <https://mirror.fsf.org/proteanos/>

### GNU Emacs text editor version 26.3 now available

*From August 28 by Nicolas Petton*

Version 26.3 of the GNU Emacs text editor is now available. You can
retrieve the source from your nearest GNU mirror at
<https://ftpmirror.gnu.org/emacs/emacs-26.3.tar.xz> or
<https://ftpmirror.gnu.org/emacs/emacs-26.3.tar.gz>. You can get the
PGP signatures at
<https://ftp.gnu.org/gnu/emacs/emacs-26.3.tar.xz.sig> or
<https://ftp.gnu.org/gnu/emacs/emacs-26.3.tar.gz.sig>.

 * <https://www.gnu.org/software/emacs/news/NEWS.26.3>

### August GNU Emacs news 

*From August 26 by Sacha Chua*

In these issues: the latest on GNU Emacs configuration, Emacs Lisp,
EmacsConf 2019, Emacs development, and more!

 * <https://sachachua.com/blog/2019/08/2019-08-26-emacs-news/>
 * <https://sachachua.com/blog/2019/08/2019-08-19-emacs-news/>
 * <https://sachachua.com/blog/2019/08/2019-08-12-emacs-news/>
 * <https://sachachua.com/blog/2019/08/2019-08-05-emacs-news/>

### Join the FSF and friends in updating the Free Software Directory

Tens of thousands of people visit directory.fsf.org each month to
discover free software. Each entry in the Directory contains a wealth
of useful information, from basic category and descriptions to version
control, IRC channels, documentation, and licensing. The Free Software
Directory has been a great resource to software users over the past
decade, but it needs your help staying up-to-date with new and
exciting free software projects.

To help, join our weekly IRC meetings on Fridays. Meetings take place
in the #fsf channel on irc.freenode.org, and usually include a handful
of regulars as well as newcomers. Freenode is accessible from any IRC
client -- Everyone's welcome!

The next meeting is Friday, August 6, from 12pm to 3pm EDT (16:00 to
19:00 UTC). Details here:

  * <https://directory.fsf.org/wiki/Main_Page>

### LibrePlanet featured resource: Group:IceCat/Links to distributions

Every month on LibrePlanet, we highlight one resource that is
interesting and useful -- often one that could use your help.

For this month, we are highlighting Group:IceCat/Links to
distributions, which provides information about distributions of GNU
IceCat, the GNU version of the Firefox browser. You are invited to
adopt, spread and improve this important resource.

  * <https://libreplanet.org/wiki/Group:IceCat/Links_to_distributions>

Do you have a suggestion for next month's featured resource? Let us
know at <campaigns@fsf.org>.

### GNU Spotlight with Mike Gerwitz: 15 new GNU releases!

15 new GNU releases in the last month (as of August 26, 2019):

* [aspell-0.60.7](https://www.gnu.org/software/aspell/)
* [freeipmi-1.6.4](https://www.gnu.org/software/freeipmi/)
* [gcc-9.2.0](https://www.gnu.org/software/gcc/)
* [glibc-2.30](https://www.gnu.org/software/glibc/)
* [gnuastro-0.10](https://www.gnu.org/software/gnuastro/)
* [gsl-2.6](https://www.gnu.org/software/gsl/)
* [help2man-1.47.11](https://www.gnu.org/software/help2man/)
* [libffcall-2.2](https://www.gnu.org/software/libffcall/)
* [libmicrohttpd-0.9.66](https://www.gnu.org/software/libmicrohttpd/)
* [mit-scheme-10.1.10](https://www.gnu.org/software/mit-scheme/)
* [nano-4.4](https://www.gnu.org/software/nano/)
* [parallel-20190822](https://www.gnu.org/software/parallel/)
* [pycdio-2.1.0](https://www.gnu.org/software/libcdio/)
* [stow-2.3.1](https://www.gnu.org/software/stow/)
* [unifont-12.1.03](https://www.gnu.org/software/unifont/)

For announcements of most new GNU releases, subscribe to the info-gnu
mailing list: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

To download: nearly all GNU software is available from
<https://ftp.gnu.org/gnu/>, or preferably one of its mirrors from
<https://www.gnu.org/prep/ftp.html>.  You can use the URL
<https://ftpmirror.gnu.org/> to be automatically redirected to a
(hopefully) nearby and up-to-date mirror.

A number of GNU packages, as well as the GNU operating system as a
whole, are looking for maintainers and other assistance: please see
<https://www.gnu.org/server/takeaction.html#unmaint> if you'd like to
help.  The general page on how to help GNU is at
<https://www.gnu.org/help/help.html>.

If you have a working or partly working program that you'd like to
offer to the GNU project as a GNU package, see
<https://www.gnu.org/help/evaluation.html>.

As always, please feel free to write to us at <maintainers@gnu.org>
with any GNUish questions or suggestions for future installments.

### FSF events and other free software events

 * September 4-6, Madrid, Spain, [GNU Hackers Meeting](https://www.fsf.org/events/event-20190904-madrid-gnuhackersmeeting)
 * September 7-8, Prague, Czechia, [ActivityPub Conference](https://dustycloud.org/blog/activitypub-conf-2019/)
 * September 7-13, Milan, Italy, [Akademy 2019](https://akademy.kde.org/2019)
 * September 16-20, Huntsville, AL, USA, [GNU Radio Conference 2019 (GRCon)](https://www.gnuradio.org/grcon/grcon19/)
 * November 2, 2019, online, [EmacsConf 2019](https://emacsconf.org/2019/)

### Thank GNUs!

We appreciate everyone who donates to the Free Software Foundation,
and we'd like to give special recognition to the folks who have
donated $500 or more in the last month.

  * <https://www.gnu.org/thankgnus/2019supporters.html>

This month, a big Thank GNU to:

* John Sullivan (not our executive director)
* Judicaël Courant
* Marinos Yannikos

You can add your name to this list by donating at
<https://donate.fsf.org/>.

### GNU copyright contributions

Assigning your copyright to the Free Software Foundation helps us
defend the GNU GPL and keep software free. The following individuals
have assigned their copyright to the FSF in the past month:

* Anders Kalør (GNU Radio)
* Charles Jackson (Emacs)
* Felix E. Klee (Emacs)
* Gustaf Waldemarson (Emacs)
* Jorge Navarro (Emacs)
* Dr. Matthias Kretz (GCC)
* Nicholas Strauss (Emacs)
* Nick Wynja (Mailman)
* RT-RK Computer Based Systems LLC (Binutils)
* Štěpán Němec (Emacs)

Want to see your name on this list? Contribute to GNU and assign your
copyright to the FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Translations of the *Free Software Supporter*

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2019/septiembre>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2019/septembre>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em português. Para ver a
versão em português, clique aqui:
<https://www.fsf.org/free-software-supporter/2019/setembro>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### Take action with the FSF!

Contributions from thousands of individual members enable the FSF's
work. You can contribute by joining at <https://my.fsf.org/join>. If
you're already a member, you can help refer new members (and earn some
rewards) by adding a line with your member number to your email
signature like:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

The FSF is always looking for volunteers
(<https://www.fsf.org/volunteer>). From rabble-rousing to hacking,
from issue coordination to envelope stuffing -- there's something here
for everybody to do. Also, head over to our campaigns section
(<https://www.fsf.org/campaigns>) and take action on software patents,
Digital Restrictions Management (DRM), free software adoption,
OpenDocument, and more.


###

Copyright © 2019 Free Software Foundation, Inc.

This work is licensed under the Creative Commons Attribution 4.0
Unported License. To view a copy of this license, visit
<https://creativecommons.org/licenses/by/4.0/>.

