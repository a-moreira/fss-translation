# Suporte ao Software Livre
edição 137, Setembro de 2019

*Adicione <info@fsf.org> ao seu catálogo de endereços para garantir o recebimento de nossos futuros e-mails.*

Bem-vindo(a) ao Suporte ao Software Livre, um resumo de notícias e
ações mensal da Free Software Foundation (FSF) -- sendo lido por você
e mais 211.363 outros ativistas. São 1.098 a mais em relação ao mês
passado!

### Junte-se a nós no Dia Internacional Contra o Gerenciamento de Restrições Digitais (DRM, na sigla em inglês), em 12 de outubro

*4 de setembro*

[A Defective by Design](https://www.defectivebydesign.org/) está
chamando você para se opor ao gerenciamento digital de restrições e se
juntar a nós em comemoração ao Dia Internacional Contra o DRM (IDAD)
no dia 12 de outubro de 2019. Fique de olho em
[defectivebydesign.org](https://www.defectivebydesign.org/) para mais
informações.

## ÍNDICE

* Alexandre Oliva entra para o conselho de diretores da Free Software Foundation
* Inscrições antecipadas abertas para o seminário sobre licenciamento da FSF, em 16 de outubro em Raleigh, Carolina do Norte
* Viagens de RMS em maio de 2019: fotos de Aalborg e Copenhaguen
* O denunciante da Agência de Segurança Nacional dos EUA (NSA, na sigla em inglês), Edward Snowden, quer ajudar a combater a vigilância
* Seu carro conversa com o fabricante. Os advogados também querem que ele converse com você.
* Polônia leva a luta contra filtros de upload ao Tribunal de Justiça Europeu
* Coisas que você não sabia sobre o GNU Readline
* A HOPE precisa de sua ajuda
* A Amazon realmente está pouco se f-(bipe), hein?
* Tudo o que você precisa saber sobre a Ring, empresa de câmeras de vigilância da Amazon
* Campanha de <em>crowdfunding</em> para o "Hacking for the Commons" ou "La bataille du Libre"
* A Conferência ActivityPub será realizada em setembro em Praga, República Tcheca
* Resumo da GNOME West Coast Hackfest 2019
* Os chamados fornos "inteligentes" têm ligados durante a noite e preaquecidos a 200 graus
* GCC 9.2 lançado
* FSF hospedando um espelho do ProteanOS
* Editor de texto GNU Emacs versão 26.3 agora disponível
* Notícias de agosto do GNU Emacs
* Junte-se à FSF e amigos na atualização do Diretório de Software Livre
* Recurso em destaque na LibrePlanet: Group:IceCat/Links to distributions
* Holofote GNU com Mike Gerwitz: 15 novos lançamentos GNU!
* Próximos eventos da FSF e de software livre
* Agradecimentos do GNU!
* Contribuições para o copyright do GNU
* Traduções do *Free Software Supporter*
* Entre em ação com a FSF!

Leia esse boletim <em>on-line</em> aqui:
<https://www.fsf.org/free-software-supporter/2019/september>

Incentive seus amigos a se inscrever e nos ajudar a aumentar nosso
público adicionando nosso <em>widget</em> de assinante ao seu <em>site</em>.


  * Inscreva-se: <https://www.fsf.org/free-software-supporter/subscribe>
  * Widget: <https://www.fsf.org/associate/widget>

Perdeu uma notícia? Você pode recuperá-la nos boletins passados em
<https://www.fsf.org/free-software-supporter>.

Deseja ler este boletim traduzido para outro idioma? Role
até o final, para ler o *Suporte* em inglês, francês ou espanhol.

***

### Alexandre Oliva entra para o conselho de diretores da Free Software Foundation

*28 de agosto*

Ativista do software livre de longa data e fundador da [FSF Latin
América](https://www.fsfla.org/ikiwiki/), Oliva traz décadas de
experiência no movimento do software livre para o conselho da FSF. Na
comunidade, ele é especialmente respeitado por ser o desenvolvedor
chefe do [Projeto GNU
Linux-libre](https://www.fsfla.org/ikiwiki/selibre/linux-libre/), uma
versão do kernel Linux que remove todos os bits não livres do 
código-fonte do kernel, permitindo que usuários de todo o mundo
executem versões livres do sistema operacional GNU/Linux, e é um
programa de importância vital para a causa da liberdade do software.
Por seu profundo compromisso e trabalho incansável no software livre,
Oliva foi o destinatário do [Prêmio Progresso do Software Livre, de
2016](https://www.fsf.org/news/securedrop-and-alexandre-oliva-are-2016-free-software-awards-winners)
concedido anualmente pela FSF.

 * <https://www.fsf.org/news/alexandre-oliva-joins-free-software-foundation-board-of-directors>

### Inscrições antecipadas abertas para o seminário sobre licenciamento da FSF, em 16 de outubro em Raleigh, Carolina do Norte

*29 de agosto*

O Laboratório de Licenciamento e Conformidade da Free Software
Foundation (FSF) se orgulha de anunciar que [as inscrições já estão
abertas](https://www.fsf.org/events/cle-seminar-20191016-raleigh) para
o Seminário de Educação Jurídica Continuada (CLE) sobre Cumprimento da
GPL e Ética Legal, que será realizado na quarta-feira, 16 de outubro
de 2019, no Centro de Convenções Raleigh, na Carolina do Norte.

 * <https://www.fsf.org/news/early-registration-open-for-fsfs-licensing-seminar-on-oct-16-in-raleigh-nc>

### Viagens de RMS em maio de 2019: fotos de Aalborg e Copenhaguen

*31 de julho*

O presidente da Free Software Foundation, Richard Stallman (RMS),
esteve na Dinamarca em maio de 2019. Depois de uma visita à praia na
vizinha Slettestrand no dia anterior, RMS foi para Aalborg, onde
proferiu seu discurso “Software livre e sua liberdade” na Universidade
de Aalborg (AAU), dia 6 de maio.

 * <https://www.fsf.org/blogs/rms/photo-blog-2019-may-aalborg-copenhagen>

### O denunciante da Agência de Segurança Nacional dos EUA (NSA), Edward Snowden, quer ajudar a combater a vigilância

*3 de agosto, por Aaron Kesel*

O denunciante Edward Snowden está de volta às notícias, anunciando
planos para um livro de memórias, bem como sua oposição à vigilância
corporativa por meio empresas que usam software proprietário e Serviço
como Substituto de Software, como Instagram, Facebook e YouTube. Este
é um bom momento para revisitar sua palestra de abertura do
LibrePlanet 2016 em
<https://media.libreplanet.org/u/libreplanet/m/libreplanet-2016-the-last-lighthouse-3d51/>.

 * <https://www.activistpost.com/2019/08/nsa-whistleblower-edward-snowden-says-facebook-instagram-spying-on-customers-wants-to-help-combat-surveillance.html>

### Seu carro conversa com o fabricante. Os advogados também querem que ele converse com você.

*6 de agosto, por Adrian Ma*

Se seu carro tiver menos de dez anos, é provável que ele esteja
equipado com "telemática" -- basicamente, computadores que permitem ao
fabricante rastrear via <em>wireless</em>, o desempenho de um carro e
enviar notificações quando algo precisa ser consertado. Em alguns
casos, essas notificações podem até recomendar a uma concessionária
próxima para que faça os reparos. O Massachusetts Right to Repair
Coalition propõe que as montadoras sejam obrigadas a dar aos
proprietários de carros acesso aos dados dos carros em tempo real, o
que é uma ótima ideia -- exceto que a proposta deles é fazer isso
através de um aplicativo móvel não-livre. Que nojo!

Isso faz parte das práticas opressivas da Internet das Picadas
(Internet of Stings, em inglês, como um nome aprimorado de "Internet
das Coisas" -- "Internet of Things", em inglês): fazendo você se
comunicar com os produtos que você possui através do servidor do
fabricante. O certo é exigir que o carro tenha uma interface apenas de
dados, como um <em>slot</em> para colocar um <em>chip</em> de memória
e o carro gravar os dados nele. Em seguida, você pode mostrar esses
dados a uma oficina independente quando você deseja, mesmo se você
desconectou a antena de dados do celular e a antena do GPS, logo após
a compra do carro. Aplaudimos qualquer esforço em direção à liberdade
do usuário, mas vamos garantir que tudo seja feito da maneira certa!

 * <https://www.wbur.org/bostonomix/2019/08/06/right-to-repair-ballot-measure>

### Polônia leva a luta contra filtros de upload ao Tribunal de Justiça Europeu

*26 de agosto, por Étienne Gonnu*

A Polônia está contestando no Tribunal de Justiça Europeu (EUCJ, na
sigla em inglês), a legalidade do Artigo 17 da Diretiva de Direitos
Autorais, votada em março de 2019 pelo Parlamento Europeu. Mais
especificamente, o estado membro argumenta que as disposições que
impõem filtros de <em>upload</em> são contrários ao artigo 11 da Carta
dos Direitos Fundamentais da União Europeia. Por outras palavras, as
disposições infringem desproporcionalmente a liberdade de expressão,
uma postura que a April tem regularmente defendida, juntamente com
muitas outras organizações.

 * <https://www.april.org/en/poland-brings-fight-against-upload-filters-european-court-justice>

### Coisas que você não sabia sobre o GNU Readline

*22 de agosto, por Chet Ramey*

GNU Readline é uma pequena e despretensiosa biblioteca de software, na
qual eu confiei por anos sem perceber que ela estava lá. Dezenas de
milhares de pessoas provavelmente a usam todos os dias sem pensar
nisso. Se você usa o shell do Bash, sempre que você completa
automaticamente um nome de arquivo ou move o cursor dentro de uma
única linha de entrada de texto, ou pesquisa o histórico de seus
comandos anteriores, você está usando a GNU Readline.

 * <https://twobithistory.org/2019/08/22/readline.html>

### A HOPE precisa de sua ajuda

*1º de agosto*

A conferência HOPE - Hackers no Planeta Terra - precisa de sua ajuda!
Os custos do hotel estão tornando proibitiva a realização da
conferência e eles precisam de suas sugestões para garantir que este
evento continue acontecendo. Envie um e-mail para <hope@hope.net> com
suas sugestões.

 * <https://hope.net/>

### A Amazon realmente está pouco se f-(bipe), hein?

*2 de agosto, por Adam Clark Estes*

Sim, pessoas reais estão ouvindo o que o assistente de voz grava,
incluindo seus momentos mais particulares, e até que ponto a Amazon,
Apple e Google permitem que você desative a opção, varia. Contratantes
da Microsoft também estão [ouvindo suas chamadas do
Skype](https://www.vice.com/pt_BR/article/xweqbq/microsoft-contractors-listen-to-skype-calls).
E é claro que nada disso é remotamente transparente -- porque você não
tem permissão para examinar o software proprietário que controla como
esses dispositivos funcionam.

 * <https://gizmodo.com/amazon-really-doesn-t-give-a-shit-huh-1836912275>

### Tudo o que você precisa saber sobre a Ring, empresa de câmeras de vigilância da Amazon

*8 de agosto, por Caroline Haskins*

Como a Ring faz parceria com a polícia? Existem preocupações com a
privacidade de campainhas de porta da empresa? O que é o aplicativo
Neighbours? Que funções estão ocultas no software proprietário da Ring
que você não pode examinar ou controlar? Saiba o que a *Motherboard*
descobriu quando examinou 1.800 páginas de dados públicos.

 * <https://www.vice.com/en_us/article/qvg48d/everything-you-need-to-know-about-ring-amazons-surveillance-camera-company?xyz>

### Campanha de <em>crowdfunding</em> para o "Hacking for the Commons" ou "La bataille du Libre"

*14 de agosto*

No coração da tecnologia, duas questões estão agora em conflito: desde
o anos 80, os princípios emancipatórios do movimento do software livre
e dos movimentos culturais começaram a atacar os movimentos que
promoviam exclusividade e interesses “privados” com a defesa dos
direitos de propriedade intelectual. Software livre, sementes livres,
medicamentos livres, conhecimento livre: focando na liberdade,
cooperação e compartilhamento, esses movimentos visam restaurar a
autonomia e o poder do usuário, levando a um mundo livre de patentes,
em benefício do bem comum.

O documentário "Hacking for the Commons" ou "La bataille du Libre"
aborda todos esses problemas, mas eles precisam da sua ajuda: para
descobrir como contribuir para a campanha de <em>crowdfunding</em>
sem usar JavaScript proprietário, envie um e-mail para
<contact@labatailledulibre.org>!

 * <https://www.labatailledulibre.org/en/the-movie/>

### A Conferência ActivityPub será realizada em setembro em Praga, República Tcheca

*22 de julho, por Christopher Lemmer Webber*

A Conferência ActivityPub oferece a você a oportunidade de discutir o
presente e futuro do ActivityPub, a líder mundial no padrão Web em
federação social. Este evento de dois dias incluirá apresentações de
palestras no sábado, seguidas de uma "inconferência" estruturada
espontaneamente no domingo.

Fomos informados de que todos os ingressos já foram reservados, mas se
você estiver interessado em aprender para onde o ActivityPub está indo,
fique de olho no <em>site</em>: as sessões serão gravadas e também
poderão ser transmitidas ao vivo.

 * <https://dustycloud.org/blog/activitypub-conf-2019/>

### Resumo da GNOME West Coast Hackfest 2019

*14 de agosto, por GNOME Foundation*

A West Coast Hackfest 2019, realizado em Portland, Oregon, EUA, entre
os dias 18 e 21 de julho, reunindo membros de várias equipes,
incluindo programadores, membros da equipe de documentação e da equipe
de engajamento. A West Coast Hackfest 2019 foi aceito como um evento
de sucesso pelos participantes. Links para blogs com mais detalhes
sobre o trabalho realizado podem ser encontrados na [página wiki da
West Coast Hackfest](https://wiki.gnome.org/Hackfests/WestCoastHackfest).
A GNOME Foundation quer agradecer a todas as pessoas que fizeram um
evento de sucesso!

 * <https://www.gnome.org/news/2019/08/west-coast-hackfest-2019-summary/>

### Os chamados fornos "inteligentes" têm ligados durante a noite e preaquecidos a 200 graus

*14 de agosto, por Ashley Carman*

Pelo menos três fornos "inteligentes" da June se acenderam no meio da
noite e aqueceram até 200 graus Celsius ou mais. Dado que 33% dos
incêndios domésticos são causados pelo que o Corpo de Bombeiros da
cidade de Nova York chama de "cozimento autônomo", esse não é um
perigo pequeno!

Obviamente, os problemas práticos são apenas uma das razões pelas
quais nos opomos a softwares proprietários na "Internet das Coisas".
O fato de os proprietários não terem permissão para mexer com esses
fornos "inteligentes" e resolver o problema por conta própria não é
apenas um aborrecimento, ou mesmo um perigo para suas casas e vidas:
é uma violação de seus direitos.

 * <https://www.theverge.com/2019/8/14/20802774/june-smart-oven-remote-preheat-update-user-error>

### GCC 9.2 lançado

*12 de agosto, por GCC*

O Projeto GNU e os desenvolvedores do GCC têm o prazer de anunciar o
lançamento do GCC 9.2. Esta versão é uma versão de correção de erros,
contendo correções para regressões no GCC 9.1 em relação às versões
anteriores do GCC.

 * <https://gcc.gnu.org/gcc-9/>

### FSF hospedando um espelho do ProteanOS

*7 de agosto*

Estamos agora hospedando um espelho de ProteanOS em
<https://mirror.fsf.org/proteanos>. ProteanOS é uma das distribuições
GNU/Linux livres listadas
[aqui](https://www.gnu.org/distros/free-distros.html).

 * <https://mirror.fsf.org/proteanos/>

### Editor de texto GNU Emacs versão 26.3 agora disponível

*28 de agosto, por Nicolas Petton*

A versão 26.3 do editor de texto GNU Emacs está agora disponível. Você
pode obter o código-fonte do espelho mais próximo a você em
<https://ftpmirror.gnu.org/emacs/emacs-26.3.tar.xz> ou
<https://ftpmirror.gnu.org/emacs/emacs-26.3.tar.gz>. Você pode obter
as assinaturas PGP em
<https://ftp.gnu.org/gnu/emacs/emacs-26.3.tar.xz.sig> ou
<https://ftp.gnu.org/gnu/emacs/emacs-26.3.tar.gz.sig>.

 * <https://www.gnu.org/software/emacs/news/NEWS.26.3>

### Notícias de agosto do GNU Emacs

*26 de agosto, por Sacha Chua*

Nestas edições: o mais recente sobre a configuração do GNU Emacs,
Emacs Lisp, EmacsConf 2019, desenvolvimento do Emacs e muito mais!

 * <https://sachachua.com/blog/2019/08/2019-08-26-emacs-news/>
 * <https://sachachua.com/blog/2019/08/2019-08-19-emacs-news/>
 * <https://sachachua.com/blog/2019/08/2019-08-12-emacs-news/>
 * <https://sachachua.com/blog/2019/08/2019-08-05-emacs-news/>

### Junte-se à FSF e amigos na atualização do Diretório de Software Livre

Dezenas de milhares de pessoas visitam directory.fsf.org todos os
meses para descobrir <em>software</em> livre. Cada registro no
Diretório contém uma grande quantidade de informações úteis, desde
categorias básicas e descrições até controle de versão, canais de IRC,
documentação e licenciamento. O Diretório de Software Livre tem sido
um ótimo recurso para usuários de <em>software</em> na última década,
mas precisa de sua ajuda para manter-se atualizado com novos e
interessantes projetos de <em>software</em> livre.

Para ajudar, participe das nossas reuniões semanais do IRC às
sextas-feiras. As reuniões acontecem no canal #fsf no irc.freenode.org
e geralmente incluem um punhado de regulares, bem como recém-chegados.
O Freenode é acessível a partir de qualquer cliente de IRC -- Todos
são bem-vindos!

A próxima reunião é sexta-feira, 6 de setembro, das 13h às 16h BRT
(16h à 19h UTC). Detalhes aqui:

  * <https://directory.fsf.org/wiki/Main_Page>

### Recurso em destaque na LibrePlanet: Group:IceCat/Links to distributions

Todos os meses na LibrePlanet, destacamos um recurso que é útil e
interessante -- geralmente um recurso que poderia receber sua ajuda.

Para este mês, estamos destacando Group:IceCat/Links to distributions,
que fornece informações sobre distribuições do GNU IceCat, a versão do
GNU para o navegador Firefox. Você está convidado para adotar,
espalhar e melhorar este importante recurso.

  * <https://libreplanet.org/wiki/Group:IceCat/Links_to_distributions>

Você tem uma sugestão para o recurso em destaque do próximo mês?
Nos avise em <campaigns@fsf.org>.

### Holofote GNU com Mike Gerwitz: 15 novos lançamentos GNU!

15 novos lançamentos GNU neste último mês (até 26 de agosto de 2019):

* [aspell-0.60.7](https://www.gnu.org/software/aspell/)
* [freeipmi-1.6.4](https://www.gnu.org/software/freeipmi/)
* [gcc-9.2.0](https://www.gnu.org/software/gcc/)
* [glibc-2.30](https://www.gnu.org/software/glibc/)
* [gnuastro-0.10](https://www.gnu.org/software/gnuastro/)
* [gsl-2.6](https://www.gnu.org/software/gsl/)
* [help2man-1.47.11](https://www.gnu.org/software/help2man/)
* [libffcall-2.2](https://www.gnu.org/software/libffcall/)
* [libmicrohttpd-0.9.66](https://www.gnu.org/software/libmicrohttpd/)
* [mit-scheme-10.1.10](https://www.gnu.org/software/mit-scheme/)
* [nano-4.4](https://www.gnu.org/software/nano/)
* [parallel-20190822](https://www.gnu.org/software/parallel/)
* [pycdio-2.1.0](https://www.gnu.org/software/libcdio/)
* [stow-2.3.1](https://www.gnu.org/software/stow/)
* [unifont-12.1.03](https://www.gnu.org/software/unifont/)

Para anúncios da maioria dos novos lançamentos GNU, inscreva-se
na lista de discussão info-gnu:
<https://lists.gnu.org/mailman/listinfo/info-gnu>.

Para baixar: quase todo <em>software</em> GNU está disponível em
<https://ftp.gnu.org/gnu/>, ou, de preferência, em um dos espelhos em
<https://www.gnu.org/prep/ftp.html>. Você pode usar a URL
<https://ftpmirror.gnu.org/> para ser redirecionado automaticamente
para um espelho próximo e atualizado.

Vários pacotes GNU, assim como o sistema operacional GNU como um
todo, estão procurando mantenedores e outras formas de auxílio: veja
<https://www.gnu.org/server/takeaction.html#unmaint> se você quiser
ajudar. A página geral sobre como ajudar o GNU está em
<https://www.gnu.org/help/help.html>.

Se você tem um programa que funcionando, totalmente ou parcialmente,
que gostaria de oferecer ao projeto GNU como um pacote GNU, veja
<https://www.gnu.org/help/evaluation.html>.

Como sempre, sinta-se à vontade para escrever para nós em
<maintainers@gnu.org> com quaisquer perguntas sobre o GNU
ou sugestões para futuras edições.

### Próximos eventos da FSF e de software livre

 * 4-6 de setembro, Madrid, Espanha, [GNU Hackers Meeting](https://www.fsf.org/events/event-20190904-madrid-gnuhackersmeeting)
 * 7-8 de setembro, Praga, República Techeca, [ActivityPub Conference](https://dustycloud.org/blog/activitypub-conf-2019/)
 * 7-13 de setembro, Milão, Itália, [Akademy 2019](https://akademy.kde.org/2019)
 * 16-20 de setembro, Huntsville, AL, EUA, [GNU Radio Conference 2019 (GRCon)](https://www.gnuradio.org/grcon/grcon19/)
 * 2 de novembro, 2019, online, [EmacsConf 2019](https://emacsconf.org/2019/)

### Agradecimentos do GNU!

Agradecemos a todos que doam à Free Software Foundation e gostaríamos
de dar um reconhecimento especial às pessoas que doaram US$ 500,00 ou
mais no último mês.

  * <https://www.gnu.org/thankgnus/2019supporters.html>

Neste mês, um grande Agradecimento do GNU para:

* John Sullivan (não nosso diretor executivo)
* Judicaël Courant
* Marinos Yannikos

Você pode adicionar seu nome a essa lista doando em
<https://donate.fsf.org/>.

### Contribuições para o copyright do GNU

Atribuir seus direitos autorais (_copyright_) à Free Software
Foundation nos ajuda a defender a GPL e manter o <em>software</em>
livre. As seguintes pessoas atribuíram seus direitos autorais à FSF
no mês passado:

* Anders Kalør (GNU Radio)
* Charles Jackson (Emacs)
* Felix E. Klee (Emacs)
* Gustaf Waldemarson (Emacs)
* Jorge Navarro (Emacs)
* Dr. Matthias Kretz (GCC)
* Nicholas Strauss (Emacs)
* Nick Wynja (Mailman)
* RT-RK Computer Based Systems LLC (Binutils)
* Štěpán Němec (Emacs)

Quer ver seu nome nesta lista? Contribua para o GNU e atribua seus
direitos autorais à FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Traduções do *Free Software Supporter*

Free Software Supporter is available in English. To see the English
version, click here:
<https://www.fsf.org/free-software-supporter/2019/setembro>

**To change the user's preferences and receive the next issues of
Supporter in English, click here:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2019/septiembre>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2019/septembre>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### Entre em ação com a FSF!

Contribuições de milhares de membros individuais possibilitam trabalho
da FSF. Você pode contribuir unindo-se em <https://my.fsf.org/join>.
Se você já é um membro, você pode ajudar a indicar novos membros
(e ganhar algumas recompensas) adicionando uma linha com seu número
de membro à sua assinatura de e-mail como:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

A FSF está sempre procurando voluntários
(<https://www.fsf.org/volunteer>). Da agitação ao hacking, da
coordenação de problemas ao preenchimento de envelopes -- há algo
aqui para todo mundo fazer. Além disso, dirija-se à nossa seção de
campanhas (<https://www.fsf.org/campaigns>) e entre em ação contra
patentes de <em>software</em>, Gestão Digital de Restrições (DRM),
adoção de <em>software</em> livre, OpenDocument, Associação da
Indústria de Gravação da América (RIAA), e mais.


###

Copyright © 2019 Free Software Foundation, Inc.

Esta obra está licenciada sob uma licença Creative Commons
Atribuição 4.0 Não Adaptada. Para ver uma cópia desta licença, visite
<https://creativecommons.org/licenses/by/4.0/deed.pt_BR>.

