# fss-translation

[![build status](https://gitlab.com/rafaelff/fss-translation/badges/master/pipeline.svg)](https://gitlab.com/rafaelff/fss-translation/badges/master/pipeline.svg)

Project to coordinate the Brazilian Portuguese localization of
[Free Software Supporter](https://www.fsf.org/fss/). <br>
See the HTML-rendered translation at
https://rafaelff.gitlab.io/fss-translation

Projeto para coordenar a localização para português brasileiro do
[Suporte ao Software Livre](https://www.fsf.org/fss/). <br>
Veja a tradução renderizada em HTML em
https://rafaelff.gitlab.io/fss-translation

## Status de tradução:

<details open>
  <summary>
      <strong>[2020](https://www.fsf.org/free-software-supporter/2020)</strong>
  </summary>

|   Mês     |                     Arquivo markdown                        |                Link para artigo na FSF                            |   Status    |
|-----------|-------------------------------------------------------------|-------------------------------------------------------------------|-------------|
| Janeiro   | [january_2020_pt-BR.mdwn](2020/january_2020_pt-BR.mdwn)     | https://www.fsf.org/free-software-supporter/2020/janeiro          | Concluído   |
| Fevereiro | [february_2020_pt-BR.mdwn](2020/february_2020_pt-BR.mdwn)   | https://www.fsf.org/free-software-supporter/2020/fevereiro        | Concluído   |
| Março     | [march_2020_pt-BR.mdwn](2020/march_2020_pt-BR.mdwn)         | https://www.fsf.org/free-software-supporter/2020/marco            | Concluído   |
| Abril     | [april_2020_pt-BR.mdwn](2020/april_2020_pt-BR.mdwn)         | https://www.fsf.org/free-software-supporter/2020/abril-p          | Concluído   |
| Maio      | [may_2020_pt-BR.mdwn](2020/may_2020_pt-BR.mdwn)             | https://www.fsf.org/free-software-supporter/2020/maio             | Concluído   |
| Junho     |                                                             |                                                                   |             |
| Julho     |                                                             |                                                                   |             |
| Agosto    |                                                             |                                                                   |             |
| Setembro  |                                                             |                                                                   |             |
| Outubro   |                                                             |                                                                   |             |
| Novembro  |                                                             |                                                                   |             |
| Dezembro  |                                                             |                                                                   |             |

</details>

<details>
  <summary>
      <strong>[2019](https://www.fsf.org/free-software-supporter/2019)</strong>
  </summary>


|   Mês     |                     Arquivo markdown                        |                Link para artigo na FSF                            |   Status    |
|-----------|-------------------------------------------------------------|-------------------------------------------------------------------|-------------|
| Janeiro   | [january_2019_pt-BR.mdwn](2019/january_2019_pt-BR.mdwn)     | https://www.fsf.org/free-software-supporter/2019/janeiro          | Concluído   |
| Fevereiro | [february_2019_pt-BR.mdwn](2019/february_2019_pt-BR.mdwn)   | https://www.fsf.org/free-software-supporter/2019/fevereiro        | Concluído   |
| Março     | [march_2019_pt-BR.mdwn](2019/march_2019_pt-BR.mdwn)         | https://www.fsf.org/free-software-supporter/2019/marco            | Concluído   |
| Abril     | [april_2019_pt-BR.mdwn](2019/april_2019_pt-BR.mdwn)         | https://www.fsf.org/free-software-supporter/2019/abril-portuguese | Concluído   |
| Maio      | [may_2019_pt-BR.mdwn](2019/may_2019_pt-BR.mdwn)             | https://www.fsf.org/free-software-supporter/2019/maio             | Concluído   |
| Junho     | [june_2019_pt-BR.mdwn](2019/june_2019_pt-BR.mdwn)           | https://www.fsf.org/free-software-supporter/2019/free-software-supporter-edicao-134-junho-2019  | Concluído   |
| Julho     | [july_2019_pt-BR.mdwn](2019/july_2019_pt-BR.mdwn)           | https://www.fsf.org/free-software-supporter/2019/julho            | Concluído   |
| Agosto    | [august_2019_pt-BR.mdwn](2019/august_2019_pt-BR.mdwn)       | https://www.fsf.org/free-software-supporter/2019/agosto-pr        | Concluído   |
| Setembro  | [september_2019_pt-BR.mdwn](2019/september_2019_pt-BR.mdwn) | https://www.fsf.org/free-software-supporter/2019/setembro         | Concluído   |
| Outubro   | [october_2019_pt-BR.mdwn](2019/october_2019_pt-BR.mdwn)     | https://www.fsf.org/free-software-supporter/2019/outubro          | Concluído   |
| Novembro  | [november_2019_pt-BR.mdwn](2019/november_2019_pt-BR.mdwn)   | https://www.fsf.org/free-software-supporter/2019/novembro         | Concluído   |
| Dezembro  | [december_2019_pt-BR.mdwn](2019/december_2019_pt-BR.mdwn)   | https://www.fsf.org/free-software-supporter/2019/dezembro         | Concluído   |

</details>

<details>
  <summary>
    <strong>[2018](https://www.fsf.org/free-software-supporter/2018)</strong>
  </summary>

|   Mês     |                     Arquivo markdown                                 |                Link para artigo na FSF                    |   Status    |
|-----------|----------------------------------------------------------------------|-----------------------------------------------------------|-------------|
| Maio      | [may_2018_pt-BR.mdwn](2018/may_2018_pt-BR.mdwn)             | https://www.fsf.org/free-software-supporter/2018/maio              | Concluído   |
| Junho     | [june_2018_pt-BR.mdwn](2018/june_2018_pt-BR.mdwn)           | https://www.fsf.org/free-software-supporter/2018/junho             | Concluído   |
| Julho     | [july_2018_pt-BR.mdwn](2018/july_2018_pt-BR.mdwn)           | https://www.fsf.org/free-software-supporter/2018/julho             | Concluído   |
| Agosto    | [august_2018_pt-BR.mdwn](2018/august_2018_pt-BR.mdwn)       | https://www.fsf.org/free-software-supporter/2018/agosto-portuguese | Concluído   |
| Setembro  | [september_2018_pt-BR.mdwn](2018/september_2018_pt-BR.mdwn) | https://www.fsf.org/free-software-supporter/2018/setembro          | Concluído   |
| Outubro   | [october_2018_pt-BR.mdwn](2018/october_2018_pt-BR.mdwn)     | https://www.fsf.org/free-software-supporter/2018/outubro           | Concluído   |
| Novembro  | [november_2018_pt-BR.mdwn](2018/november_2018_pt-BR.mdwn)   | https://www.fsf.org/free-software-supporter/2018/novembro          | Concluído   |
| Dezembro  | [december_2018_pt-BR.mdwn](2018/december_2018_pt-BR.mdwn)   | https://www.fsf.org/free-software-supporter/2018/dezembro          | Concluído   |

</details>
